# Videogame Town


Whether it's a turn-based RPG or action game, I was thinking I could learn to make towns that player characters
can walk around, talk to non-player characters, open chests, and open/exit buildings. So in Unity I made a
2D town from the top-perspective where players can do just that. This could be implemented in a variety of genres.
And upon reflection, I thought it was a rewarding experience learning about how to make player inventories, movement,
etc. and how those functions relate to C# classes. 